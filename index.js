var math = require('./lib/math');
var jokes = require('./lib/jokes');

var app = {};

app.config = {
    timeBetweenJokes: 1000
};

app.printAJoke = function () {
    var arrayOfJokes = jokes.all();

    var numberOfJokes = arrayOfJokes.length;

    var randomInteger = math.getRandomInteger(0, numberOfJokes);

    var selectedJoke = arrayOfJokes[randomInteger];

    console.log(selectedJoke);
}

app.indefiniteLoop = function () {
    setInterval(app.printAJoke, app.config.timeBetweenJokes)
}

app.indefiniteLoop();