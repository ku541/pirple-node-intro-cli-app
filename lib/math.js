var math = {};

math.getRandomInteger = function (min, max) {
    min = Number.isInteger(min) && Math.sign(min) === 1 ? min : 0;
    max = Number.isInteger(max) && Math.sign(max) === 1 ? max : 0;

    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min) + min);
}

module.exports = math;