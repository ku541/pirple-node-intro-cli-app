var fs = require('fs');

var jokes = {};

jokes.all = function () {
    var stringOfJokes = fs.readFileSync(__dirname + '/jokes.txt', 'utf8');

    var arrayOfJokes = stringOfJokes.split(/\r?\n/);

    return arrayOfJokes;
}

module.exports = jokes;